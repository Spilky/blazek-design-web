$(document).ready(function(){
    //init nette ajax
    $.nette.init();

    //init fancybox
    $(".fancybox").fancybox({
        helpers: {
            title: {
                type: 'inside'
            }
        },
        beforeShow : function(){
            if ($(this.element).data("description") !== undefined && $(this.element).data("description").trim()) {
                this.title =  this.title + " - " + $(this.element).data("description");
            }
        }
    });
});