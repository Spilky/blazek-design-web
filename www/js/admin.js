$(document).ready(function(){
    initLibraries();
});

$.nette.ext('initLibraries', {
    complete: function () {
        initLibraries();
    }
});

$.nette.ext('closeModal', {
    complete: function (jqXHR, status, settings) {
        if (!settings.nette) {
            return;
        }

        var modal = settings.nette.el.data('close-modal');
        if (modal) {
            $(modal).modal('hide');
        }
    }
});

function initLibraries() {
    //Initialize Select2 Elements
    $(".select2").select2();

    $('.datepicker').datetimepicker({
        locale: 'cs',
        format: 'DD.M.YYYY'
    });
}