<?php
/**
 * Created by PhpStorm.
 * User: Spilky
 * Date: 16.01.2016
 * Time: 14:48
 */

namespace App\Model\Service;

use App\Model\Entity\Brand;

class BrandedTablesManager
{
    /** @var Brand */
    private static $brand = null;

    public static function getBrand()
    {
        return self::$brand;
    }

    public static function setBrand($brand)
    {
        self::$brand = $brand;
    }
}