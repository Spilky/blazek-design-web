<?php
/**
 * Created by PhpStorm.
 * User: Spilky
 * Date: 27.12.2015
 * Time: 12:14
 */

namespace App\Model\Service;

use Nette\Utils\Image;

class ImageManager
{
    /** @var string */
    private $directory;
    /** @var string|null */
    private $subDirectory;
    /** @var string|null */
    private $watermark;

    /**
     * ImageManager constructor.
     * @param string $directory
     * @param string|null $watermark
     */
    public function __construct($directory, $watermark = null)
    {
        $this->directory = $directory;
        $this->watermark = $watermark;

        if (substr($this->directory, -1) != '/') {
            $this->directory .= '/';
        }
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param mixed $subDirectory
     */
    public function setSubDirectory($subDirectory)
    {
        $this->subDirectory = $subDirectory;

        if (substr($this->subDirectory, 0, 1) == '/') {
            $this->subDirectory = substr($this->subDirectory, 1);
        }

        if (substr($this->subDirectory, -1) != '/') {
            $this->subDirectory .= '/';
        }
    }

    private function getPath()
    {
        return $this->directory . $this->subDirectory;
    }

    /**
     * @param $image
     * @param null $name
     * @param int $width
     * @param int $height
     * @param bool $useWatermark
     * @return null|string
     * @throws \Exception
     */
    public function saveImage($image, $name = null, $width = 1024, $height = 768, $useWatermark = true)
    {
        $name = $this->checkName($name);

        $image = Image::fromString($image);
        $image->resize($width, $height, Image::SHRINK_ONLY);

        if ($useWatermark) {
            $this->applyWatermark($image);
        }

        $image->save($this->getPath() . $name);
        return $name;
    }

    /**
     * @param $image
     * @param null $name
     * @param int $width
     * @param int $height
     * @param bool $useWatermark
     * @return null|string
     * @throws \Exception
     */
    public function saveMiniature($image, $name = null, $width = 400, $height = 400, $useWatermark = false)
    {
        $name = $this->checkName($name);

        $image = Image::fromString($image);
        $image->resize($width, $height, Image::EXACT);

        if ($useWatermark) {
            $this->applyWatermark($image);
        }

        $image->save($this->getPath() . $name);
        return $name;
    }

    /**
     * @param $image
     * @param null $name
     * @param int $width
     * @param int $height
     * @param bool $useWatermark
     * @return null|string
     * @throws \Exception
     */
    public function saveBlackWhiteMiniature($image, $name = null, $width = 400, $height = 400, $useWatermark = false)
    {
        $name = $this->checkName($name);

        $image = Image::fromString($image);
        $image->resize($width, $height, Image::EXACT);

        if ($useWatermark) {
            $this->applyWatermark($image);
        }

        $image->filter(IMG_FILTER_GRAYSCALE);
        $image->save($this->getPath() . $name);
        return $name;
    }

    /**
     * @param $name
     * @return string
     * @throws \Exception
     */
    private function checkName($name)
    {
        if (is_null($name)) {
            $name = $this->generateName();
        } else {
            if (is_file($this->getPath() . $name)) {
                throw new \Exception('Takovýto obrázek (soubor) již existuje.');
            }
        }

        return $name;
    }

    /**
     * @param string $suffix
     * @return string
     */
    private function generateName($suffix = 'jpg')
    {
        do {
            $name = '';
            for($i = 0; $i < 10; $i++){
                $name .= rand(0, 9);
            }
            $name .= '.' . $suffix;

        } while (is_file($this->getPath() . $name));

        return $name;
    }

    /**
     * @param Image $image
     * @return Image
     */
    private function applyWatermark(Image $image)
    {
        $logo = Image::fromFile($this->directory . 'logo.png');
        $image->place($logo, $image->width - $logo->width - 20, $image->height - $logo->height - 20, 50);
        return $image;
    }
}