<?php

namespace App\Model\Repository;

use App\Model\Entity\AbstractBrandedEntity;
use App\Model\Entity\Brand;
use App\Model\Service\BrandedTablesManager;
use Kdyby\Doctrine\QueryBuilder;

abstract class AbstractBrandedRepository extends AbstractRepository
{
    /**
     * @param AbstractBrandedEntity|AbstractBrandedEntity[] $entity
     */
    public function insert($entity)
    {
        if (is_array($entity)) {
            $entities = $entity;
        } else {
            $entities = array($entity);
        }

        foreach ($entities as $entity) {
            if (is_null($entity->getBrand())) {
                $brand = $this->entityManager->getRepository(Brand::getClassName())
                    ->find(BrandedTablesManager::getBrand());
                $entity->setBrand($brand);
            }

            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();
    }

    /**
     * @param array $values
     * @param array $where
     */
    public function updateWhere($values, $where = array())
    {
        $where['brand'] = $this->entityManager->getRepository(Brand::getClassName())
            ->find(BrandedTablesManager::getBrand());
        parent::updateWhere($values, $where);
    }

    /**
     * @return AbstractBrandedEntity[]
     */
    public function getBrandedAll()
    {
        $parameters['brand'] = BrandedTablesManager::getBrand();
        return $this->entityRepository->findBy($parameters);
    }

    /**
     * @param array $parameters
     * @return AbstractBrandedEntity[]
     */
    public function getByParameters($parameters)
    {
        $parameters['brand'] = BrandedTablesManager::getBrand();
        return parent::getByParameters($parameters);
    }

    /**
     * @param array $parameters
     * @return null|AbstractBrandedEntity
     */
    public function getOneByParameters($parameters)
    {
        $parameters['brand'] = BrandedTablesManager::getBrand();
        return parent::getOneByParameters($parameters);
    }

    /**
     * @return QueryBuilder
     */
    public function getQB()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('table')->from($this->entityRepository->getClassName(), 'table')
            ->join('table.brand', 'b')
            ->where('b.id = :brand')
            ->setParameter('brand', BrandedTablesManager::getBrand());

        return $qb;
    }
}
