<?php

namespace App\Model\Repository;

use App\Model\Entity\Service;
use Kdyby\Doctrine\EntityManager;

class ServiceRepository extends AbstractBrandedRepository
{
    /**
     * ServiceRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Service::getClassName());
    }

    /**
     * @return Service[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param int $id
     * @return Service
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Service[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }
}
