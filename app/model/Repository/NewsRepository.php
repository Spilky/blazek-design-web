<?php

namespace App\Model\Repository;

use App\Model\Entity\News;
use Kdyby\Doctrine\EntityManager;

class NewsRepository extends AbstractBrandedRepository
{
    /**
     * NewsRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(News::getClassName());
    }

    /**
     * @return News[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param int $id
     * @return null|News
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return News[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param array $parameters
     * @return null|News
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}
