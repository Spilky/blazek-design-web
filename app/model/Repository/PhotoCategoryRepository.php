<?php

namespace App\Model\Repository;

use App\Model\Entity\PhotoCategory;
use Kdyby\Doctrine\EntityManager;

class PhotoCategoryRepository extends AbstractBrandedRepository
{
    /**
     * PhotoCategoryRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(PhotoCategory::getClassName());
    }

    /**
     * @return PhotoCategory[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @return PhotoCategory[]
     */
    public function getBrandedAll()
    {
        return parent::getBrandedAll();
    }

    /**
     * @param int $id
     * @return null|PhotoCategory
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return PhotoCategory[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }
}
