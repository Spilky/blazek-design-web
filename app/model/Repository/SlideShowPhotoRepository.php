<?php

namespace App\Model\Repository;

use App\Model\Entity\Photo;
use App\Model\Entity\SlideShowPhoto;
use Kdyby\Doctrine\EntityManager;

class SlideShowPhotoRepository extends AbstractBrandedRepository
{
    /**
     * SlideShowPhotoRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(SlideShowPhoto::getClassName());
    }

    /**
     * @return Photo[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @return Photo[]
     */
    public function getBrandedAll()
    {
        return parent::getBrandedAll();
    }

    /**
     * @param int $id
     * @return Photo
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Photo[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }
}
