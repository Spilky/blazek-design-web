<?php

namespace App\Model\Repository;

use App\Model\Entity\Brand;
use Kdyby\Doctrine\EntityManager;

class BrandRepository extends AbstractRepository
{
    /**
     * BrandRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Brand::getClassName());
    }

    /**
     * @return Brand[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param int $id
     * @return null|Brand
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Brand[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param array $parameters
     * @return null|Brand
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}
