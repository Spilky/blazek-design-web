<?php

namespace App\Model\Repository;

use App\Model\Entity\Photo;
use App\Model\Service\BrandedTablesManager;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryBuilder;

class PhotoRepository extends AbstractBrandedRepository
{
    /**
     * PhotoRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Photo::getClassName());
    }

    /**
     * @return Photo[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @return Photo[]
     */
    public function getBrandedAll()
    {
        return parent::getBrandedAll();
    }

    /**
     * @param int $id
     * @return Photo
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Photo[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @return QueryBuilder
     */
    public function getQB()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select('table')->from($this->entityRepository->getClassName(), 'table')
            ->join('table.brand', 'b')
            ->leftJoin('table.categories', 'c')
            ->where('b.id = :brand')
            ->setParameter('brand', BrandedTablesManager::getBrand());

        return $qb;
    }
}
