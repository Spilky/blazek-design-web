<?php

namespace App\Model\Repository;

use App\Model\Entity\Contact;
use Kdyby\Doctrine\EntityManager;

class ContactRepository extends AbstractBrandedRepository
{
    /**
     * ContactRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Contact::getClassName());
    }

    /**
     * @return Contact[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @return Contact[]
     */
    public function getBrandedAll()
    {
        return parent::getBrandedAll();
    }

    /**
     * @param int $id
     * @return null|Contact
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Contact[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param array $parameters
     * @return null|Contact
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}
