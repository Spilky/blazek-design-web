<?php

namespace App\Model\Repository;

use App\Model\Entity\Page;
use Kdyby\Doctrine\EntityManager;

class PageRepository extends AbstractBrandedRepository
{
    const NEWS_PAGE_URL = 'novinky';
    const SERVICES_PAGE_URL = 'sluzby';
    const GALLERY_PAGE_URL = 'galerie';
    const CONTACTS_PAGE_URL = 'kontakty';

    /**
     * PageRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entityRepository = $this->entityManager->getRepository(Page::getClassName());
    }

    /**
     * @return Page[]
     */
    public function getAll()
    {
        return parent::getAll();
    }

    /**
     * @param int $id
     * @return null|Page
     */
    public function getById($id)
    {
        return parent::getById($id);
    }

    /**
     * @param array $parameters
     * @return Page[]
     */
    public function getByParameters($parameters)
    {
        return parent::getByParameters($parameters);
    }

    /**
     * @param array $parameters
     * @return null|Page
     */
    public function getOneByParameters($parameters)
    {
        return parent::getOneByParameters($parameters);
    }
}
