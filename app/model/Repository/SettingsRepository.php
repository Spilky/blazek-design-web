<?php

namespace App\Model\Repository;

use App\Model\Entity\Settings;
use Kdyby\Doctrine\EntityManager;
use Doctrine\ORM\EntityRepository;

class SettingsRepository extends AbstractRepository
{
    /** @var EntityRepository */
    private $settings;

    /**
     * SettingsRepository constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->settings = $this->entityManager->getRepository(Settings::getClassName());
    }
}
