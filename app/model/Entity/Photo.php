<?php

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Photo extends AbstractBrandedEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="PhotoCategory", inversedBy="photos")
     */
    protected $categories;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="string")
     */
    protected $image;

    /**
     * @ORM\Column(type="string")
     */
    protected $miniature;

    /**
     * @ORM\Column(type="string")
     */
    protected $miniatureBW;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $homePage = false;

    /**
     * Photo constructor.
     * @param null|array $parameters
     */
    public function __construct($parameters = null)
    {
        parent::__construct($parameters);
        $this->categories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return PhotoCategory[]|ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param ArrayCollection $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getMiniature()
    {
        return $this->miniature;
    }

    /**
     * @param string $miniature
     */
    public function setMiniature($miniature)
    {
        $this->miniature = $miniature;
    }

    /**
     * @return string
     */
    public function getMiniatureBW()
    {
        return $this->miniatureBW;
    }

    /**
     * @param string $miniatureBW
     */
    public function setMiniatureBW($miniatureBW)
    {
        $this->miniatureBW = $miniatureBW;
    }

    /**
     * @return boolean
     */
    public function getHomePage()
    {
        return $this->homePage;
    }

    /**
     * @param boolean $homePage
     */
    public function setHomePage($homePage)
    {
        $this->homePage = $homePage;
    }
}
