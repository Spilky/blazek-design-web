<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractBrandedEntity extends AbstractEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Brand")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=false)
     */
    protected $brand;

    /**
     * AbstractBrandedEntity constructor.
     * @param null|array $parameters
     */
    public function __construct($parameters = null)
    {
        parent::__construct($parameters);
    }

    /**
     * @return Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }
}
