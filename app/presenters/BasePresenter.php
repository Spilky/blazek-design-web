<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;

/**
 * Base presenter for all presenters.
 */
abstract class BasePresenter extends Presenter
{
}
