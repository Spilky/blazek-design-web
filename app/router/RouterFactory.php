<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{
    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;

        $filterTable = [
            'presenter' => [
                Route::VALUE => 'Homepage',
                Route::FILTER_TABLE => [
                    'kategorie-galerie' => 'CategoryGallery',
                    'kontakty' => 'Contacts',
                    'galerie' => 'Gallery',
                    'novinky' => 'News',
                    'profil' => 'Profile',
                    'sluzby' => 'Services',
                    'nastaveni' => 'Settings',
                    'rozcestnik' => 'Signpost',
                    'slideshow' => 'Slideshow',
                    'uzivatele' => 'Users'
                ],
            ],
            'action' => [
                Route::VALUE => 'default',
                Route::FILTER_TABLE => [
                    'pridat' => 'add',
                    'editovat' => 'edit',
                    'nastaveni' => 'settings'
                ]
            ],
            'id' => NULL,
        ];

        $admin = new RouteList('Admin');
        $admin[] = new Route('prihlasit', 'Sign:in');
        $admin[] = new Route('odhlasit', 'Sign:out');
        $admin[] = new Route('prihlasit', 'Sign:forgottenPassword');
        $admin[] = new Route('admin/[b-<idBrand [0-9]+>/]<presenter>/<action>[/<id>]', $filterTable);
        $router[] = $admin;

        $front = new RouteList('Front');
        $front[] = new Route('[<subUrl>/]<presenter>/<action>[/<id>]', $filterTable);
        $router[] = $front;

        return $router;
    }
}
