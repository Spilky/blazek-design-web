<?php

namespace App\AdminModule\DataGrids;

use App\Model\Entity\SlideShowPhoto;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\SlideShowPhotoRepository;
use App\Model\Service\ImageManager;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;
use Nette\Utils\Html;
use Nette\Utils\Image;

class SlideShowPhotosDataGrid extends Object
{
    /** @var SlideShowPhotoRepository */
    private $photoRepository;
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;
    /** @var ImageManager */
    private $imageManager;

    /**
     * SlideShowPhotosDataGrid constructor.
     * @param SlideShowPhotoRepository $photoRepository
     * @param PhotoCategoryRepository $photoCategoryRepository
     * @param ImageManager $imageManager
     */
    public function __construct
    (
        SlideShowPhotoRepository $photoRepository,
        PhotoCategoryRepository $photoCategoryRepository,
        ImageManager $imageManager
    ) {
        $this->photoRepository = $photoRepository;
        $this->photoCategoryRepository = $photoCategoryRepository;
        $this->imageManager = $imageManager;
    }
    
    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('title' => 'ASC'));

        $grid->setModel($this->photoRepository->getQB());

        $grid->addColumnText('image', 'Obrázek')
            ->setCustomRender(function ($item) {
                /** @var SlideShowPhoto $item */
                $image = $item->getImage();
                if (empty($image)) {
                    return '';
                }
                $img = Html::el('img');
                $img->src = '/images/slideshow/' . $image;
                $img->style = 'max-width: 200px;';

                $a = Html::el('a');
                $a->href = '/images/slideshow/' . $image;
                $a->class = 'fancybox';
                $a->title = $item->getTitle();
                $a->data('description', $item->getDescription());

                $a->add($img);
                return $a;
            });

        $grid->addColumnText('title', 'Titulek')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $categories = PhotoCategoryRepository::getIdIndexedArrayOfNames($this->photoCategoryRepository->getBrandedAll());
        $categories[''] = '';
        asort($categories);
        $grid->addColumnText('category', 'Kategorie')
            ->setCustomRender(function ($item) {
                /** @var SlideShowPhoto $item */
                return $item->getCategory()->getName();
            })
            ->setSortable()
            ->setFilterSelect($categories);

        $grid->addColumnText('description', 'Popisek')
            ->setTruncate(50);

        $grid->addColumnText('resolution', 'Rozlišení')
            ->setCustomRender(function ($item) {
                /** @var SlideShowPhoto $item */
                $image = $item->getImage();
                if (empty($image)) {
                    return '';
                }
                $image = Image::fromFile(
                    $this->imageManager->getDirectory() . 'slideshow/' . $item->getImage()
                );
                $resolution = $image->getWidth() . 'x' . $image->getHeight() . 'px';
                $ratio = round($image->getWidth() / $image->getHeight(), 2);
                return $resolution . ' (' . $ratio . ')' ;
            });

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit fotografii {$item->title} ze slideshow?";
            });

        return $grid;
    }

}
