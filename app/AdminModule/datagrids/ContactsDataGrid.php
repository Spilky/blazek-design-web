<?php

namespace App\AdminModule\DataGrids;

use App\Model\Repository\ContactRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;

class ContactsDataGrid extends Object
{
    /** @var ContactRepository */
    private $contactRepository;

    /**
     * ContactsDataGrid constructor.
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('name' => 'ASC'));

        $grid->setModel($this->contactRepository->getQB());

        $grid->addColumnText('name', 'Jméno')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addColumnEmail('email', 'E-mail')
            ->setSortable()
            ->setFilterDate();

        $grid->addColumnText('telephone', 'Telefon')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit kontakt {$item->name}?";
            });

        return $grid;
    }
}
