<?php

namespace App\AdminModule\DataGrids;

use App\Model\Entity\Photo;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\PhotoRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;
use Nette\Utils\Html;

class PhotosDataGrid extends Object
{
    /** @var PhotoRepository */
    private $photoRepository;
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;

    /**
     * PhotosDataGrid constructor.
     * @param PhotoRepository $photoRepository
     * @param PhotoCategoryRepository $photoCategoryRepository
     */
    public function __construct(PhotoRepository $photoRepository, PhotoCategoryRepository $photoCategoryRepository)
    {
        $this->photoRepository = $photoRepository;
        $this->photoCategoryRepository = $photoCategoryRepository;
    }
    
    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('title' => 'ASC'));

        $grid->setModel($this->photoRepository->getQB());

        $grid->addColumnText('image', 'Obrázek')
            ->setCustomRender(function ($item) {
                /** @var Photo $item */
                $miniature = $item->getMiniature();
                $image = $item->getImage();
                if (empty($miniature) || empty($image)) {
                    return '';
                }
                $img = Html::el('img');
                $img->src = '/images/galerie/' . $miniature;
                $img->style = 'max-width: 200px;';

                $a = Html::el('a');
                $a->href = '/images/galerie/' . $image;
                $a->class = 'fancybox';
                $a->title = $item->getTitle();
                $a->data('description', $item->getDescription());

                $a->add($img);
                return $a;
            });

        $grid->addColumnText('title', 'Titulek')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $categories = PhotoCategoryRepository::getIdIndexedArrayOfNames($this->photoCategoryRepository->getBrandedAll());
        $categories[''] = '';
        asort($categories);
        $grid->addColumnText('categories', 'Kategorie')
            ->setCustomRender(function ($item) {
                /** @var Photo $item */;
                $categories = array();
                foreach ($item->getCategories() as $category) {
                    $categories[] = $category->getName();
                }
                return implode(', ', $categories);
            })
            ->setSortable()
            ->setFilterSelect($categories)
            ->setCondition(function ($item) {
                return is_null($item) ?
                    null :
                    array('c.id', '= ?', $item);
            });

        $grid->addColumnText('description', 'Popisek')
            ->setTruncate(50);

        $hp = $grid->addColumnText('homePage', 'H')
            ->setCustomRender(function ($item) {
                /** @var Photo $item */
                if ($item->getHomePage()) {
                    return '<i class="fa fa-eye" data-toggle="tooltip" data-placement="top"
                            title="Zobrazuje se na hlavní straně"></i>';
                } else {
                    return '<i class="fa fa-eye-slash" data-toggle="tooltip" data-placement="top"
                            title="Nezobrazuje se na hlavní straně"></i>';
                }
            });

        $hp->getCellPrototype()->style('width: 1%; text-align: center;');
        $hp->getHeaderPrototype()->style('width: 1%; text-align: center');

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit fotografii {$item->title}?";
            });

        return $grid;
    }

}
