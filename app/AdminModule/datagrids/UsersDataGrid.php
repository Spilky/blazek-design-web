<?php

namespace App\AdminModule\DataGrids;

use App\Model\Repository\UserRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;

class UsersDataGrid extends Object
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * UsersDataGrid constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('name' => 'ASC'));

        $grid->setModel($this->userRepository->getQB());

        $grid->addColumnText('name', 'Jméno')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addColumnEmail('email', 'E-mail')
            ->setSortable()
            ->setFilterDate();

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit uživatele {$item->name}?";
            });

        return $grid;
    }
}
