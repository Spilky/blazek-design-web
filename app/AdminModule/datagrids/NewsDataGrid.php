<?php

namespace App\AdminModule\DataGrids;

use App\Model\Entity\News;
use App\Model\Repository\NewsRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;
use Nette\Utils\Html;

class NewsDataGrid extends Object
{
    /** @var NewsRepository */
    private $newsRepository;

    /**
     * NewsDataGrid constructor.
     * @param NewsRepository $newsRepository
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }

    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('added' => 'DESC'));

        $grid->setModel($this->newsRepository->getQB());

        $grid->addColumnText('title', 'Titulek')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addColumnText('image', 'Obrázek')
            ->setCustomRender(function ($item) {
                /** @var News $item */
                $miniature = $item->getMiniature();
                $image = $item->getImage();
                if (empty($miniature) || empty($image)) {
                    return '';
                }
                $img = Html::el('img');
                $img->src = '/images/novinky/' . $miniature;
                $img->style = 'max-width: 200px;';

                $a = Html::el('a');
                $a->href = '/images/novinky/' . $image;
                $a->class = 'fancybox';
                $a->title = $item->getTitle();

                $a->add($img);
                return $a;
            });

        $grid->addColumnDate('added', 'Přidáno')
            ->setDateFormat('d.m.Y')
            ->setSortable();

        $grid->addColumnText('text', 'Text')
            ->setTruncate(50);

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit novinku {$item->title}?";
            });

        return $grid;
    }
}
