<?php

namespace App\AdminModule\DataGrids;

use App\Model\Entity\Service;
use App\Model\Repository\ServiceRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;
use Nette\Utils\Html;

class ServicesDataGrid extends Object
{
    /** @var ServiceRepository */
    private $serviceRepository;

    /**
     * ServicesDataGrid constructor.
     * @param ServiceRepository $serviceRepository
     */
    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
    }

    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('name' => 'ASC'));

        $grid->setModel($this->serviceRepository->getQB());

        $grid->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addColumnText('image', 'Obrázek')
            ->setCustomRender(function ($item) {
                /** @var Service $item */
                $image = $item->getImage();
                if (empty($image)) {
                    return '';
                }
                $img = Html::el('img');
                $img->src = '/images/sluzby/' . $item->getImage();
                return $img;
            });

        $grid->addColumnText('text', 'Text')
            ->setTruncate(50);

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit službu {$item->name}?";
            });

        return $grid;
    }

}
