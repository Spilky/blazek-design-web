<?php

namespace App\AdminModule\DataGrids;

use App\Model\Repository\PhotoCategoryRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Object;

class PhotoCategoriesDataGrid extends Object
{
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;

    /**
     * PhotoCategoriesDataGrid constructor.
     * @param PhotoCategoryRepository $photoCategoryRepository
     */
    public function __construct(PhotoCategoryRepository $photoCategoryRepository)
    {
        $this->photoCategoryRepository = $photoCategoryRepository;
    }
    
    /**
     * @return Grid
     */
    public function create()
    {
        $grid = new Grid();
        $grid->translator->lang = 'cs';
        $grid->setFilterRenderType(Filter::RENDER_INNER);
        $grid->setDefaultSort(array('name' => 'ASC'));

        $grid->setModel($this->photoCategoryRepository->getQB());

        $grid->addColumnText('name', 'Jméno')
            ->setSortable()
            ->setFilterText()
            ->setSuggestion();

        $grid->addActionHref('edit', 'Editovat')
            ->setIcon('pencil');

        $grid->addActionHref('delete', 'Smazat')
            ->setIcon('trash')
            ->setConfirm(function($item) {
                return "Opravdu chcete odstranit kategorii {$item->name}?" .
                    " Odstraníte tak i všechny fotografie v této kategorii.";
            });

        return $grid;
    }
}
