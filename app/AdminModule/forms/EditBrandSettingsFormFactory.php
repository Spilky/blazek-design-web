<?php

namespace App\AdminModule\Forms;

use App\Model\Repository\BrandRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class EditBrandSettingsFormFactory extends Object
{
    /** @var BrandRepository */
    private $brandRepository;

    /**
     * EditPageSettingsFormFactory constructor.
     * @param BrandRepository $pageRepository
     */
    public function __construct(BrandRepository $pageRepository)
    {
        $this->brandRepository = $pageRepository;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addTextArea('text', 'Text:')
            ->setAttribute('rows', '15');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $id = $values['id'];
            unset($values['id']);
            $this->brandRepository->updateWhere($values, array('id' => $id));
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
