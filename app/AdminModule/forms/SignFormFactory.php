<?php

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class SignFormFactory extends Object
{
    /** @var User */
    private $user;

    /**
     * SignFormFactory constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    
    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;
        $form->addText('email', 'E-mail:')
            ->setRequired('Vložte prosím e-mail.')
            ->setAttribute('placeholder', 'E-mail');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Vložte prosím heslo.')
            ->setAttribute('placeholder', 'Heslo');

        $form->addCheckbox('remember', ' Zůstat přihlášen');

        $form->addSubmit('submit', 'Přihlásit se');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if ($values['remember']) {
            $this->user->setExpiration('14 days', FALSE);
        } else {
            $this->user->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->user->login($values['email'], $values['password']);
        } catch (AuthenticationException $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
