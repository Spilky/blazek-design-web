<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\Contact;
use App\Model\Repository\ContactRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditContactFormFactory extends Object
{
    /** @var ContactRepository */
    private $contactRepository;

    /**
     * AddEditContactFormFactory constructor.
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }
    
    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name', 'Jméno:')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('email', 'E-mail:')
            ->setRequired('Vlože prosím e-mail')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');

        $form->addText('telephone', 'Telefon:')
            ->setRequired('Vložte prosím telefonní číslo.')
            ->addRule(Form::INTEGER, 'Vložte prosím validní telefonní číslo.');

        $form->addCheckbox('contactForm', 'Odesílat tomuto kontaktu dotazy z kontaktního formuláře.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if (empty($values['id'])) {
            $news = new Contact($values);
        }

        try {
            if (empty($values['id'])) {
                $this->contactRepository->insert($news);
            } else {
                $id = $values['id'];
                unset($values['id']);
                $this->contactRepository->updateWhere($values, array('id' => $id));
            }
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
