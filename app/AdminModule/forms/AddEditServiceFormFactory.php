<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\Service;
use App\Model\Repository\ServiceRepository;
use App\Model\Service\ImageManager;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditServiceFormFactory extends Object
{
    /** @var ServiceRepository */
    private $serviceRepository;
    /** @var ImageManager */
    private $imageManager;
    /** @var boolean */
    private $editing = false;

    /**
     * AddEditServiceFormFactory constructor.
     * @param ServiceRepository $serviceRepository
     * @param ImageManager $imageManager
     */
    public function __construct(ServiceRepository $serviceRepository, ImageManager $imageManager)
    {
        $this->serviceRepository = $serviceRepository;
        $this->imageManager = $imageManager;
    }

    /**
     * @param boolean $editing
     */
    public function setEditing($editing)
    {
        $this->editing = $editing;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name', 'Název:')
            ->setRequired('Vložte prosím název.');

        $imageInput = $form->addUpload('image', 'Obrázek:');

        if ($this->editing) {
            $imageInput->addCondition(Form::FILLED)
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);
        } else {
            $imageInput->setRequired('Obrázek musí být vložen.')
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);
        }

        $form->addTextArea('text', 'Text:')
            ->setAttribute('rows', '7')
            ->setRequired('Vlože prosím text.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if ($values['image']->isOk()) {
            $this->imageManager->setSubDirectory('sluzby');
            $values['image'] = $this->imageManager->saveMiniature(
                file_get_contents($values['image']),
                null,
                300,
                300,
                false
            );
        } else {
            unset($values['image']);
        }

        if (empty($values['id'])) {
            $service = new Service($values);
        }

        try {
            if (empty($values['id'])) {
                $this->serviceRepository->insert($service);
            } else {
                $id = $values['id'];
                $service = $this->serviceRepository->getById($id);
                if (isset($values['image']) &&
                    is_file($this->imageManager->getDirectory() . 'sluzby/' . $service->getImage())) {
                    unlink($this->imageManager->getDirectory() . 'sluzby/' . $service->getImage());
                }
                unset($values['id']);
                $this->serviceRepository->updateWhere($values, array('id' => $id));
            }
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
