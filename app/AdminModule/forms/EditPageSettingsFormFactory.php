<?php

namespace App\AdminModule\Forms;

use App\Model\Repository\PageRepository;
use App\Model\Repository\PhotoCategoryRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class EditPageSettingsFormFactory extends Object
{
    /** @var PageRepository */
    private $pageRepository;
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;
    /** @var string */
    private $page = null;

    /**
     * EditPageSettingsFormFactory constructor.
     * @param PageRepository $pageRepository
     * @param PhotoCategoryRepository $photoCategoryRepository
     */
    public function __construct(PageRepository $pageRepository, PhotoCategoryRepository $photoCategoryRepository)
    {
        $this->pageRepository = $pageRepository;
        $this->photoCategoryRepository = $photoCategoryRepository;
    }

    /**
     * @param string $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addTextArea('text', 'Text:')
            ->setAttribute('rows', '15');

        if ($this->page) {
            $form = $this->setAdditionalInputs($form);
        }

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        try {
            $id = $values['id'];
            unset($values['id']);
            $this->pageRepository->updateWhere($values, array('id' => $id));
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }

    /**
     * @param Form $form
     * @return Form
     */
    private function setAdditionalInputs($form)
    {
        $customInfo = $form->addContainer('customInfo');
        if ($this->page == PageRepository::CONTACTS_PAGE_URL) {
            $contactAddress = $customInfo->addContainer('contactAddress');

            $contactAddress->addText('companyName', 'Název společnosti');

            $contactAddress->addText('street', 'Ulice a číslo popisné');

            $contactAddress->addText('zip', 'PSČ');

            $contactAddress->addText('city', 'Město');

            $billingAddress = $customInfo->addContainer('billingAddress');

            $billingAddress->addText('companyName', 'Název společnosti');

            $billingAddress->addText('street', 'Ulice a číslo popisné');

            $billingAddress->addText('zip', 'PSČ');

            $billingAddress->addText('city', 'Město');

            $billingAddress->addText('ic', 'IČ');

            $billingAddress->addText('dic', 'DIČ');
        } elseif ($this->page == PageRepository::GALLERY_PAGE_URL) {
            $categories = ['0' => 'Vše'];
            $categories += PhotoCategoryRepository::getIdIndexedArrayOfNames(
                $this->photoCategoryRepository->getBrandedAll()
            );
            $customInfo->addSelect('category', 'Defaultní kategorie:', $categories);
        }

        return $form;
    }
}
