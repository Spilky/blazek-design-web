<?php

namespace App\AdminModule\Forms;

use App\Model\Repository\UserRepository;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;
use Nette\Object;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Nette\Utils\Random;
use Tracy\Debugger;

class ForgottenUserPasswordFormFactory extends Object
{
    /** @var UserRepository */
    private $userRepository;
    /** @var SmtpMailer */
    private $mailer;
    /** @var LinkGenerator */
    private $linkGenerator;
    
    public function __construct(
        UserRepository $userRepository,
        SmtpMailer $mailer,
        LinkGenerator $linkGenerator
    ) {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
        $this->linkGenerator = $linkGenerator;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('email', 'E-mail:')
            ->setRequired('Vložte prosím e-mail.')
            ->setAttribute('placeholder', 'E-mail');

        $form->addSubmit('submit', 'Potvrdit');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $user = $this->userRepository->getOneByParameters(array('email' => $values['email']));
        if (is_null($user)) {
            $form->addError('nonExistUser');
            return;
        }
        
        $values['password'] = Random::generate();
        $mail = new Message();
        $mail->setFrom('Blažek design <admin@truhlarstvijps.cz>')
            ->addTo($values['email'])
            ->setSubject('Zapomenuté heslo do administrace')
            ->setBody("Dobrý den,\nNové přihlašovací údaje do administrace na stránce " .
                $this->linkGenerator->link('Admin:Homepage:') . " jsou:\n" .
                "Přihlašovací e-mail: " . $values['email'] . "\n" .
                "Vygenerované heslo: " . $values['password']);
        $values['password'] = Passwords::hash($values['password']);
        unset($values['email']);

        try {
            $this->userRepository->updateWhere($values, array('id' => $user->getId()));
            $this->mailer->send($mail);
        } catch (\Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
