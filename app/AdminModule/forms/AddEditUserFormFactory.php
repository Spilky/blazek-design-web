<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;
use Nette\Object;
use Nette\Security\Passwords;
use Nette\Utils\Random;
use Tracy\Debugger;

class AddEditUserFormFactory extends Object
{
    /** @var UserRepository */
    private $userRepository;
    /** @var SmtpMailer */
    private $mailer;
    /** @var LinkGenerator */
    private $linkGenerator;


    public function __construct(
        UserRepository $userRepository,
        SmtpMailer $mailer,
        LinkGenerator $linkGenerator
    ) {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
        $this->linkGenerator = $linkGenerator;
    }


    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name', 'Jméno:')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('email', 'E-mail:')
            ->setRequired('Vlože prosím e-mail')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param $values
     */
    public function formSucceeded(Form $form, $values)
    {
        if (empty($values['id'])) {
            $values['password'] = Random::generate();
            $mail = new Message();
            $mail->setFrom('Blažek design <admin@truhlarstvijps.cz>')
                ->addTo($values['email'])
                ->setSubject('Registrace do administrace')
                ->setBody("Dobrý den,\nPřihlašovací údaje do administrace na stránce " .
                    $this->linkGenerator->link('Admin:Homepage:') . " jsou:\n" .
                    "Přihlašovací e-mail: " . $values['email'] . "\n" .
                    "Vygenerované heslo: " . $values['password']);
            $values['password'] = Passwords::hash($values['password']);
            $user = new User($values);
        }

        try {
            if (empty($values['id'])) {
                $this->userRepository->insert($user);
                $this->mailer->send($mail);
            } else {
                $id = $values['id'];
                unset($values['id']);
                $this->userRepository->updateWhere($values, array('id' => $id));
            }
        } catch (UniqueConstraintViolationException $e) {
            $form->addError($e);
            Debugger::log($e);
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
