<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\PhotoCategory;
use App\Model\Repository\PhotoCategoryRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditPhotoCategoryFormFactory extends Object
{
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;
    /** @var integer|null */
    private $insertedId = null;

    /**
     * AddEditPhotoCategoryFormFactory constructor.
     * @param PhotoCategoryRepository $photoCategoryRepository
     */
    public function __construct(PhotoCategoryRepository $photoCategoryRepository)
    {
        $this->photoCategoryRepository = $photoCategoryRepository;
    }
    
    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name', 'Název:')
        ->setRequired('Vložte prosím název kategorie.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('inserted');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        unset($values['inserted']);
        if (empty($values['id'])) {
            $category = new PhotoCategory($values);
        }

        try {
            if (empty($values['id'])) {
                $this->photoCategoryRepository->insert($category);
                $this->insertedId = $category->getId();
            } else {
                $this->photoCategoryRepository->updateWhere($values, array('id' => $values['id']));
            }
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }

    /**
     * @return mixed
     */
    public function getInsertedId()
    {
        return $this->insertedId;
    }
}
