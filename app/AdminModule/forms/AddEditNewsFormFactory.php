<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\News;
use App\Model\Repository\NewsRepository;
use App\Model\Service\ImageManager;
use DateTime;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditNewsFormFactory extends Object
{
    /** @var NewsRepository */
    private $newsRepository;
    /** @var ImageManager */
    private $imageManager;
    /** @var boolean */
    private $editing = false;

    /**
     * AddEditNewsFormFactory constructor.
     * @param NewsRepository $newsRepository
     * @param ImageManager $imageManager
     */
    public function __construct(NewsRepository $newsRepository, ImageManager $imageManager)
    {
        $this->newsRepository = $newsRepository;
        $this->imageManager = $imageManager;
    }

    /**
     * @param boolean $editing
     */
    public function setEditing($editing)
    {
        $this->editing = $editing;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('title', 'Titulek:')
            ->setRequired('Vložte prosím titulek.');

        $form->addText('added', 'Datum přidání')
            ->setRequired('Vložte prosím datum přidání');

        $imageInput = $form->addUpload('image', 'Obrázek:');

        if ($this->editing) {
            $imageInput->addCondition(Form::FILLED)
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);
        } else {
            $imageInput->setRequired('Obrázek musí být vložen.')
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);
        }

        $form->addTextArea('text', 'Text:')
            ->setAttribute('rows', '7')
            ->setRequired('Vlože prosím text.');

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        if ($values['image']->isOk()) {
            $this->imageManager->setSubDirectory('novinky');
            $values['miniature'] = $this->imageManager->saveMiniature(
                file_get_contents($values['image']),
                null,
                400,
                300
            );
            $values['image'] = $this->imageManager->saveImage(file_get_contents($values['image']));
        } else {
            unset($values['image']);
        }

        $values['added'] = new DateTime($values['added']);
        if (empty($values['id'])) {
            $news = new News($values);
        }

        try {
            if (empty($values['id'])) {
                $this->newsRepository->insert($news);
            } else {
                $id = $values['id'];
                $news = $this->newsRepository->getById($id);
                if (isset($values['image']) &&
                    is_file($this->imageManager->getDirectory() . 'sluzby/' . $news->getImage())) {
                    unlink($this->imageManager->getDirectory() . 'sluzby/' . $news->getImage());
                }
                unset($values['id']);
                $this->newsRepository->updateWhere($values, array('id' => $id));
            }
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
