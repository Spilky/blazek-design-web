<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\SlideShowPhoto;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\SlideShowPhotoRepository;
use App\Model\Service\ImageManager;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditSlideShowPhotoFormFactory extends Object
{
    /** @var SlideShowPhotoRepository */
    private $photoRepository;
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;
    /** @var ImageManager */
    private $imageManager;
    /** @var boolean */
    private $editing = false;

    /**
     * AddEditPhotoFormFactory constructor.
     * @param SlideShowPhotoRepository $photoRepository
     * @param PhotoCategoryRepository $photoCategoryRepository
     * @param ImageManager $imageManager
     */
    public function __construct(
        SlideShowPhotoRepository $photoRepository,
        PhotoCategoryRepository $photoCategoryRepository,
        ImageManager $imageManager
    ) {
        $this->photoRepository = $photoRepository;
        $this->photoCategoryRepository = $photoCategoryRepository;
        $this->imageManager = $imageManager;
    }

    /**
     * @param mixed $editing
     */
    public function setEditing($editing)
    {
        $this->editing = $editing;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('title', 'Titulek:');

        $categories = PhotoCategoryRepository::getIdIndexedArrayOfNames($this->photoCategoryRepository->getBrandedAll());
        $form->addSelect('category', 'Kategorie:', $categories)
            ->setRequired('Vyberte prosím kategorii.');

        $form->addTextArea('description', 'Popisek:');

        if ($this->editing) {
            $form->addUpload('image', 'Obrázek:')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                    ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);

            $form->addHidden('id');

            $form->onSuccess[] = array($this, 'editPhotoFormSucceeded');
        } else {
            $form->addUpload('image', 'Obrázek:')
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);

            $form->onSuccess[] = array($this, 'addPhotoFormSucceeded');
        }

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function addPhotoFormSucceeded(Form $form, ArrayHash $values)
    {
        if ($values['image']->isOk()) {
            $this->imageManager->setSubDirectory('slideshow');
            $values['image'] = $this->imageManager->saveImage(
                file_get_contents($values['image']),
                null,
                1920,
                INF,
                false);
        } else {
            $form->addError('imageError');
            return;
        }

        $values['category'] =$this->photoCategoryRepository->getById($values['category']);

        $photo = new SlideShowPhoto($values);

        try {
            $this->photoRepository->insert($photo);
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editPhotoFormSucceeded(Form $form, ArrayHash $values)
    {;
        if ($values['image']->isOk()) {
            $this->imageManager->setSubDirectory('slideshow');
            $values['image'] = $this->imageManager->saveImage(
                file_get_contents($values['image']),
                null,
                1920,
                INF,
                false);
        } else {
            if ($values['image']->getError() != UPLOAD_ERR_NO_FILE) {
                $form->addError('imageError');
                return;
            }

            unset($values['image']);
        }

        $values['category'] =$this->photoCategoryRepository->getById($values['category']);

        try {
            $id = $values['id'];
            $photo = $this->photoRepository->getById($id);
            if (isset($values['image']) &&
                is_file($this->imageManager->getDirectory() . 'slideshow/' . $photo->getImage())) {
                unlink($this->imageManager->getDirectory() . 'slideshow/' . $photo->getImage());
            }
            unset($values['id']);
            $this->photoRepository->updateWhere($values, array('id' => $id));
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
