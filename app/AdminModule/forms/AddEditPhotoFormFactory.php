<?php

namespace App\AdminModule\Forms;

use App\Model\Entity\Photo;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\PhotoRepository;
use App\Model\Service\ImageManager;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class AddEditPhotoFormFactory extends Object
{
    /** @var PhotoRepository */
    private $photoRepository;
    /** @var PhotoCategoryRepository */
    private $photoCategoryRepository;
    /** @var ImageManager */
    private $imageManager;
    /** @var boolean */
    private $editing = false;
    
    /**
     * AddEditPhotoFormFactory constructor.
     * @param PhotoRepository $photoRepository
     * @param PhotoCategoryRepository $photoCategoryRepository
     * @param ImageManager $imageManager
     */
    public function __construct(
        PhotoRepository $photoRepository,
        PhotoCategoryRepository $photoCategoryRepository,
        ImageManager $imageManager
    ) {
        $this->photoRepository = $photoRepository;
        $this->photoCategoryRepository = $photoCategoryRepository;
        $this->imageManager = $imageManager;
    }

    /**
     * @param mixed $editing
     */
    public function setEditing($editing)
    {
        $this->editing = $editing;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('title', 'Titulek:');

        $categories = PhotoCategoryRepository::getIdIndexedArrayOfNames($this->photoCategoryRepository->getBrandedAll());
        $form->addMultiSelect('categories', 'Kategorie:', $categories)
            ->setRequired('Vyberte prosím kategorii.');

        $form->addTextArea('description', 'Popisek:');

        $form->addSelect('homePage', 'Hlavní strana', array(1 => 'Ano', 0 => 'Ne'))
            ->setDefaultValue(0);

        if ($this->editing) {
            $form->addUpload('image', 'Obrázek:')
                ->addCondition(Form::FILLED)
                    ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                    ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);

            $form->addHidden('id');

            $form->onSuccess[] = array($this, 'editPhotoFormSucceeded');
        } else {
            $form->addMultiUpload('images', 'Obrázek:')
                ->setRequired('Obrázek musí být vložen.')
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.')
                ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 15 MB.', 15 * 1024 * 1024);

            $form->onSuccess[] = array($this, 'addPhotoFormSucceeded');
        }

        $form->addSubmit('submit', 'Uložit');

        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function addPhotoFormSucceeded(Form $form, ArrayHash $values)
    {
        $categories = $values['categories'];
        $values['categories'] = new ArrayCollection();
        foreach ($categories as $category) {
            $values['categories']->add($this->photoCategoryRepository->getById($category));
        }

        $photos = array();
        $images = $values['images'];
        unset($values['images']);
        foreach ($images as $image) {
            if ($image->isOk()) {
                $this->imageManager->setSubDirectory('galerie');
                $values['miniature'] = $this->imageManager->saveMiniature(file_get_contents($image));
                $values['miniatureBW'] = $this->imageManager->saveBlackWhiteMiniature(file_get_contents($image));
                $values['image'] = $this->imageManager->saveImage(file_get_contents($image));
            } else {
                $form->addError('imageError');
                return;
            }

            $photo = new Photo($values);
            $photo->setCategories($values['categories']);
            $photos[] = $photo;
        }

        try {
            $this->photoRepository->insert($photos);
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function editPhotoFormSucceeded(Form $form, ArrayHash $values)
    {;
        if ($values['image']->isOk()) {
            $this->imageManager->setSubDirectory('galerie');
            $values['miniature'] = $this->imageManager->saveMiniature(file_get_contents($values['image']));
            $values['miniatureBW'] = $this->imageManager->saveBlackWhiteMiniature(file_get_contents($values['image']));
            $values['image'] = $this->imageManager->saveImage(file_get_contents($values['image']));
        } else {
            if ($values['image']->getError() != UPLOAD_ERR_NO_FILE) {
                $form->addError('imageError');
                return;
            }

            unset($values['image']);
        }

        $categories = $values['categories'];
        $values['categories'] = new ArrayCollection();
        foreach ($categories as $category) {
            $values['categories']->add($this->photoCategoryRepository->getById($category));
        }

        try {
            $id = $values['id'];
            $photo = $this->photoRepository->getById($id);
            if (isset($values['image']) &&
                is_file($this->imageManager->getDirectory() . 'galerie/' . $photo->getImage())) {
                unlink($this->imageManager->getDirectory() . 'galerie/' . $photo->getImage());
            }
            if (isset($values['image']) &&
                is_file($this->imageManager->getDirectory() . 'galerie/' . $photo->getMiniature())) {
                unlink($this->imageManager->getDirectory() . 'galerie/' . $photo->getMiniature());
            }
            unset($values['id']);
            $this->photoRepository->updateWhere(((array) $values), array('id' => $id));
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
