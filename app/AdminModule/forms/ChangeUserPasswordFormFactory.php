<?php

namespace App\AdminModule\Forms;

use App\Model\Repository\UserRepository;
use Exception;
use Nette\Application\UI\Form;
use Nette\Object;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class ChangeUserPasswordFormFactory extends Object
{
    /** @var UserRepository */
    private $userRepository;

    /**
     * ChangeUserPasswordFormFactory constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addPassword('password', 'Stávající heslo:')
            ->setRequired('Vložte prosím stávající heslo.');

        $form->addPassword('newPassword', 'Nové heslo:')
            ->setRequired('Vlože prosím nové heslo.');

        $form->addPassword('newPasswordAgaing', 'Znovu heslo:')
            ->setRequired('Vlože prosím opět nové heslo pro kontrolu.')
            ->addRule(Form::EQUAL, _('V polích pro nové heslo nejsou uvedené stejné hodnoty.'), $form['newPassword']);

        $form->addSubmit('submit', 'Uložit');

        $form->addHidden('id');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }
    
    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $user = $this->userRepository->getById($values['id']);
        if (!Passwords::verify($values['password'], $user->getPassword())) {
            $form->addError('incorrectPassword');
            return;
        }

        $data['password'] = Passwords::hash($values['newPassword']);

        try {
            $this->userRepository->updateWhere($data, array('id' => $user->getId()));
        } catch (Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}
