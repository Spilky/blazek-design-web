<?php

namespace App\AdminModule\Presenters;

use App\Model\Repository\BrandRepository;
use App\Model\Service\BrandedTablesManager;
use App\Presenters\BasePresenter;

/**
 * Base presenter for all AdminModule presenters.
 */
abstract class AdminBasePresenter extends BasePresenter
{
    /** @var number @persistent */
    public $idBrand = 1;
    /** @var BrandRepository @inject */
    public $brandRepository;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isLoggedIn()) {
            if ($this->getName() == 'Admin:Sign' &&
                ($this->getAction() == 'in' || $this->getAction() == 'forgottenPassword')) {
                return;
            } else {
                $this->redirect('Sign:in');
                return;
            }
        }

        $brand = $this->brandRepository->getById($this->idBrand);
        BrandedTablesManager::setBrand($brand);
    }

    public function beforeRender()
    {
        $this->template->brands = $this->brandRepository->getAll();
    }
}
