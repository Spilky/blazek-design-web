<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\EditBrandSettingsFormFactory;
use App\Model\Service\BrandedTablesManager;
use Nette\Application\UI\Form;

class SignpostPresenter extends AdminBasePresenter
{
    /** @var EditBrandSettingsFormFactory @inject */
    public $editBrandSettingFormFactory;

    public function renderDefault()
    {

    }

    /**
     * @return Form
     */
    protected function createComponentEditBrandSettingsForm()
    {
        $form = $this->editBrandSettingFormFactory->create();
        $form->setDefaults(BrandedTablesManager::getBrand()->getAsArray());
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nastavení rozcestníku bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Nastavení rozcestníku se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }
}
