<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\UsersDataGrid;
use App\AdminModule\Forms\AddEditUserFormFactory;
use App\Model\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Grido\Grid;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class UsersPresenter extends AdminBasePresenter
{
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var AddEditUserFormFactory @inject */
    public $addEditUserFormFactory;
    /** @var UsersDataGrid @inject */
    public $usersDataGrid;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        if ($id == $this->user->id) {
            $this->flashMessage('Nelze smazat svůj uživatelský profil.', 'warning');
            $this->redirect('default');
            return;
        }

        $user = $this->userRepository->getById($id);
        if (is_null($user)) {
            throw new BadRequestException;
        }

        try {
            $this->userRepository->delete($user);
            $this->flashMessage('Uživatel úspěšně smazán.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Uživatele se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    /**

     * @return Form
     */
    protected function createComponentAddEditUserForm()
    {
        $form = $this->addEditUserFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Uživatel byl úspěšně ' . ($this->getAction() == 'pridat' ?  'vložen.' : 'editován.'),
                'success'
            );
            $this->redirect('Uzivatele:');
        };
        $form->onError[] = function ($form) {
            $errors = $form->getErrors();
            if ($errors['0'] instanceof UniqueConstraintViolationException) {
                $this->flashMessage(
                    'Uživatel s tímto e-mailem již existuje, použijte jiný.'
                    , 'warning');
            } else {
                $this->flashMessage(
                    'Uživatele se nepodařilo ' .
                    ($this->getAction() == 'pridat' ?  'vložit.' : 'editovat.') .
                    ' Zkuste to prosím znovu.'
                    , 'danger');
            }
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentUsersDataGrid()
    {
        $grid = $this->usersDataGrid->create();
        return $grid;
    }
}
