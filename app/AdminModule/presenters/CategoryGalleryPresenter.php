<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\PhotoCategoriesDataGrid;
use App\AdminModule\Forms\AddEditPhotoCategoryFormFactory;
use App\Model\Repository\PhotoCategoryRepository;
use Grido\Grid;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class CategoryGalleryPresenter extends AdminBasePresenter
{
    /** @var PhotoCategoryRepository @inject */
    public $photoCategoryRepository;
    /** @var AddEditPhotoCategoryFormFactory @inject */
    public $addEditPhotoCategoryFormFactory;
    /** @var PhotoCategoriesDataGrid @inject */
    public $photoCategoriesDataGrid;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $category = $this->photoCategoryRepository->getById($id);
        if (is_null($category)) {
            throw new BadRequestException;
        }

        $this->template->id = $id;
        $this['addEditPhotoCategoryForm']->setDefaults($category->getAsArray());
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $category = $this->photoCategoryRepository->getById($id);
        if (is_null($category)) {
            throw new BadRequestException;
        }

        try {
            $this->photoCategoryRepository->delete($category);
            $this->flashMessage('Kategorie galerie úspěšně smazána.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Kategorii galerie se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditPhotoCategoryForm()
    {
        $form = $this->addEditPhotoCategoryFormFactory->create();
        $form->onSuccess[] = function ($form) {
                $this->flashMessage(
                    'Kategorie byla úspěšně ' . ($this->getAction() == 'add' ?  'vložena.' : 'editována.'),
                    'success'
                );
                $this->redirect('CategoryGallery:');
            };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Kategorii se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                '. Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentPhotoCategoriesDataGrid()
    {
        $grid = $this->photoCategoriesDataGrid->create();
        return $grid;
    }
}
