<?php

namespace App\AdminModule\Presenters;

use App\Model\Repository\ContactRepository;
use App\Model\Repository\NewsRepository;
use App\Model\Repository\PhotoRepository;
use App\Model\Repository\ServiceRepository;

class HomepagePresenter extends AdminBasePresenter
{
    /** @var NewsRepository @inject */
    public $newsRepository;
    /** @var ServiceRepository @inject */
    public $serviceRepository;
    /** @var PhotoRepository @inject */
    public $photoRepository;
    /** @var ContactRepository @inject */
    public $contactRepository;

    public function renderDefault()
    {
        $counts = array(
            'news' => count($this->newsRepository->getBrandedAll()),
            'service' => count($this->serviceRepository->getBrandedAll()),
            'photo' => count($this->photoRepository->getBrandedAll()),
            'contact' => count($this->contactRepository->getBrandedAll())
        );
        $this->template->counts = $counts;
    }
}
