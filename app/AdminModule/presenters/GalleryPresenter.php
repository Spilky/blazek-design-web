<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\PhotosDataGrid;
use App\AdminModule\Forms\AddEditPhotoCategoryFormFactory;
use App\AdminModule\Forms\EditPageSettingsFormFactory;
use App\Model\Repository\PageRepository;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\PhotoRepository;
use App\Model\Service\ImageManager;
use Grido\Grid;
use App\AdminModule\Forms\AddEditPhotoFormFactory;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Tracy\Debugger;

class GalleryPresenter extends AdminBasePresenter
{
    /** @var PhotoRepository @inject */
    public $photoRepository;
    /** @var PhotoCategoryRepository @inject */
    public $photoCategoryRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var AddEditPhotoFormFactory @inject */
    public $addEditPhotoFormFactory;
    /** @var AddEditPhotoCategoryFormFactory @inject */
    public $addEditPhotoCategoryFormFactory;
    /** @var EditPageSettingsFormFactory @inject */
    public $editPageSettingsFormFactory;
    /** @var PhotosDataGrid @inject */
    public $photosDataGrid;
    /** @var ImageManager @inject */
    public $imageManager;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $photo = $this->photoRepository->getById($id);
        if (is_null($photo)) {
            throw new BadRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $photo = $this->photoRepository->getById($id);
        if (is_null($photo)) {
            throw new BadRequestException;
        }

        try {
            $miniature = $this->imageManager->getDirectory() . 'galerie/' . $photo->getMiniature();
            $image = $this->imageManager->getDirectory() . 'galerie/' . $photo->getImage();
            $this->photoRepository->delete($photo);

            if (is_file($miniature)) {
                unlink($miniature);
            }

            if (is_file($image)) {
                unlink($image);
            }

            $this->flashMessage('Fotografie úspěšně smazána.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Fotografii se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    public function renderAdd()
    {
        $this->template->_form = $this['addEditPhotoForm'];
    }

    /**
     * @param int $id
     */
    public function renderEdit($id)
    {
        $photo = $this->photoRepository->getById($id);

        $img = Html::el('img');
        $img->src = '/images/galerie/' . $photo->getMiniature();
        $img->style = 'max-width: 200px;';

        $a = Html::el('a');
        $a->href = '/images/galerie/' . $photo->getImage();
        $a->class = 'fancybox';
        $a->title = (!empty($photo->getTitle()) ? $photo->getTitle() . ' - ' : '') . $photo->getDescription();

        $a->add($img);

        $this->template->id = $id;
        $this->template->image = $a;
        $photo = $photo->getAsArray();
        $photo['homePage'] = intval($photo['homePage']);
        if (!empty($this['addPhotoCategoryForm']['inserted']->getValue())) {
            \Tracy\Debugger::fireLog($this['addPhotoCategoryForm']['inserted']->getValue());
            $photo['categories'] = array_merge($photo['categories'], explode('-', $this['addPhotoCategoryForm']['inserted']->getValue()));
        }
        $this['addEditPhotoForm']->setDefaults($photo);
        $this->template->_form = $this['addEditPhotoForm'];
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditPhotoForm()
    {
        $this->addEditPhotoFormFactory->setEditing($this->getAction() == 'edit');
        $form = $this->addEditPhotoFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Fotografie byla úspěšně ' . ($this->getAction() == 'add' ?  'vložena.' : 'editována.'),
                'success'
            );
            $this->redirect('Gallery:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Fotografii se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                ' Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentAddPhotoCategoryForm()
    {
        $form = $this->addEditPhotoCategoryFormFactory->create();
        $form->onSuccess[] = function (Form $form) {
            $categories = PhotoCategoryRepository::getIdIndexedArrayOfNames(
                $this->photoCategoryRepository->getBrandedAll());
            $this['addEditPhotoForm']['categories']->setItems($categories);
            $inserted = !empty($form->getValues()['inserted']) ? explode('-', $form->getValues()['inserted']) : [];
            $inserted[] = $this->addEditPhotoCategoryFormFactory->getInsertedId();
            $form->setValues(['inserted' => implode('-', $inserted), 'name' => null]);
            $this->redrawControl('addEditPhotoArea');
            $this->redrawControl('categoryPart');
            $this->redrawControl('addPhotoCategoryArea');
            $this->redrawControl('modalForm');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentEditPageSettingsForm()
    {
        $this->editPageSettingsFormFactory->setPage(PageRepository::GALLERY_PAGE_URL);
        $form = $this->editPageSettingsFormFactory->create();
        $form->setDefaults($this->pageRepository->getOneByParameters(array('url' => PageRepository::GALLERY_PAGE_URL))
            ->getAsArray());
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nastavení stránky bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Nastavení stránky se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentPhotosDataGrid()
    {
        $grid = $this->photosDataGrid->create();
        return $grid;
    }
}
