<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\ServicesDataGrid;
use App\AdminModule\Forms\EditPageSettingsFormFactory;
use App\Model\Repository\PageRepository;
use App\Model\Repository\ServiceRepository;
use App\Model\Service\ImageManager;
use Grido\Grid;
use App\AdminModule\Forms\AddEditServiceFormFactory;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ServicesPresenter extends AdminBasePresenter
{
    /** @var ServiceRepository @inject */
    public $serviceRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var AddEditServiceFormFactory @inject */
    public $addEditServiceFormFactory;
    /** @var EditPageSettingsFormFactory @inject */
    public $editPageSettingsFormFactory;
    /** @var ServicesDataGrid @inject */
    public $servicesDataGrid;
    /** @var ImageManager @inject */
    public $imageManager;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $service = $this->serviceRepository->getById($id);
        if (is_null($service)) {
            throw new BadRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $service = $this->serviceRepository->getById($id);
        if (is_null($service)) {
            throw new BadRequestException;
        }

        try {
            $this->serviceRepository->delete($service);
            $this->flashMessage('Služba úspěšně smazána.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Službu se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    /**
     * @param int $id
     */
    public function renderEdit($id)
    {
        $service = $this->serviceRepository->getById($id);

        $this->template->id = $id;
        $this->template->image = $service->getImage();
        $this['addEditServiceForm']->setDefaults($service->getAsArray());
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditServiceForm()
    {
        $this->addEditServiceFormFactory->setEditing($this->getAction() == 'edit');
        $form = $this->addEditServiceFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Služba byla úspěšně ' . ($this->getAction() == 'add' ?  'vložena.' : 'editována.'),
                'success'
            );
            $this->redirect('Services:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Službu se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                '. Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentEditPageSettingsForm()
    {
        $form = $this->editPageSettingsFormFactory->create();
        $form->setDefaults($this->pageRepository->getOneByParameters(array('url' => PageRepository::SERVICES_PAGE_URL))
            ->getAsArray());
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nastavení stránky bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Nastavení stránky se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentServicesDataGrid()
    {
        $grid = $this->servicesDataGrid->create();
        return $grid;
    }
}
