<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\AddEditUserFormFactory;
use App\AdminModule\Forms\ChangeUserPasswordFormFactory;
use App\Model\Repository\UserRepository;
use Nette;
use App\Model;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;

class ProfilePresenter extends AdminBasePresenter
{
    /** @var UserRepository @inject */
    public $userRepository;
    /** @var AddEditUserFormFactory @inject */
    public $addEditUserFormFactory;
    /** @var ChangeUserPasswordFormFactory @inject */
    public $changeUserPasswordFormFactory;

    /**
     * @throws BadRequestException
     */
    public function actionDefault()
    {
        $user = $this->userRepository->getById($this->user->id);
        if (is_null($user)) {
            throw new BadRequestException;
        }

        $this['addEditUserForm']->setDefaults($user->getAsArray());
        $this['changeUserPasswordForm']['id']->setValue($user->getId());
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditUserForm()
    {
        $form = $this->addEditUserFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Profil byl úspěšně editován.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Profil se nepodařilo editovat. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentChangeUserPasswordForm()
    {
        $form = $this->changeUserPasswordFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Heslo bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $errors = $form->getErrors();
            if ($errors[0] == 'incorrectPassword') {
                $this->flashMessage('Špatně zadané současné heslo. Zkuste to prosím znovu.', 'warning');
            } else {
                $this->flashMessage('Heslo se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
            }
        };
        return $form;
    }
}
