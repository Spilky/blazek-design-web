<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\ForgottenUserPasswordFormFactory;
use App\AdminModule\Forms\SignFormFactory;
use Nette\Application\UI\Form;

class SignPresenter extends AdminBasePresenter
{
    /** @var SignFormFactory @inject */
    public $signFormFactory;
    /** @var ForgottenUserPasswordFormFactory @inject */
    public $forgottenUserPasswordFormFactory;

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Byl jste úspěšně odhlášen.');
        $this->redirect('Sign:in');
    }

    /**
     * @return Form
     */
    protected function createComponentSignInForm()
    {
        $form = $this->signFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->redirect('Homepage:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Se zadanými údaji se nepodařilo přihlásit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentForgottenUserPasswordForm()
    {
        $form = $this->forgottenUserPasswordFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nové heslo vám bylo zasláno na email.', 'success');
            $this->redirect('Sign:in');
        };
        $form->onError[] = function ($form) {
            $errors = $form->getErrors();
            if ($errors[0] == 'nonExistUser') {
                $this->flashMessage('Uživatel s tímto e-mailem neexistuje. Zkuste to prosím znovu.', 'warning');
            } else {
                $this->flashMessage('Nepodařilo se vygenerovat a odeslat nové heslo. Zkuste to prosím znovu.', 'danger');
            }
        };
        return $form;
    }
}
