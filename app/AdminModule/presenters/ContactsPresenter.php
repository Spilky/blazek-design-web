<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\ContactsDataGrid;
use App\AdminModule\Forms\EditPageSettingsFormFactory;
use App\Model\Repository\ContactRepository;
use App\Model\Repository\PageRepository;
use App\AdminModule\Forms\AddEditContactFormFactory;
use Grido\Grid;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Tracy\Debugger;

class ContactsPresenter extends AdminBasePresenter
{
    /** @var ContactRepository @inject */
    public $contactRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var AddEditContactFormFactory @inject */
    public $addEditContactFormFactory;
    /** @var EditPageSettingsFormFactory @inject */
    public $editPageSettingsFormFactory;
    /** @var ContactsDataGrid @inject */
    public $contactsDataGrid;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $contact = $this->contactRepository->getById($id);
        if (is_null($contact)) {
            throw new BadRequestException;
        }

        $this['addEditContactForm']->setDefaults($contact->getAsArray());
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $contact = $this->contactRepository->getById($id);
        if (is_null($contact)) {
            throw new BadRequestException;
        }

        try {
            $this->contactRepository->delete($contact);
            $this->flashMessage('Kontakt úspěšně smazán.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Kontakt se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditContactForm()
    {
        $form = $this->addEditContactFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Kontakt byl úspěšně ' . ($this->getAction() == 'add' ?  'vložen.' : 'editován.'),
                'success'
            );
            $this->redirect('Contacts:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Kontakt se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                '. Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentEditPageSettingsForm()
    {
        $this->editPageSettingsFormFactory->setPage(PageRepository::CONTACTS_PAGE_URL);
        $form = $this->editPageSettingsFormFactory->create();
        $form->setDefaults($this->pageRepository->getOneByParameters(array('url' => PageRepository::CONTACTS_PAGE_URL))
            ->getAsArray());
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nastavení stránky bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Nastavení stránky se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentContactsDataGrid()
    {
        $grid = $this->contactsDataGrid->create();
        return $grid;
    }
}
