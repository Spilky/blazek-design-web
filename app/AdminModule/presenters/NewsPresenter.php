<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\NewsDataGrid;
use App\AdminModule\Forms\EditPageSettingsFormFactory;
use App\Model\Repository\NewsRepository;
use App\Model\Repository\PageRepository;
use Grido\Grid;
use App\AdminModule\Forms\AddEditNewsFormFactory;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Tracy\Debugger;

class NewsPresenter extends AdminBasePresenter
{
    /** @var NewsRepository @inject*/
    public $newsRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var AddEditNewsFormFactory @inject */
    public $addEditNewsFormFactory;
    /** @var EditPageSettingsFormFactory @inject */
    public $editPageSettingsFormFactory;
    /** @var NewsDataGrid @inject */
    public $newsDataGridFactory;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $news = $this->newsRepository->getById($id);
        if (is_null($news)) {
            throw new BadRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $news = $this->newsRepository->getById($id);
        if (is_null($news)) {
            throw new BadRequestException;
        }

        try {
            $this->newsRepository->delete($news);
            $this->flashMessage('Novinka úspěšně smazána.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Novinku se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    /**
     * @param int $id
     */
    public function renderEdit($id)
    {
        $news = $this->newsRepository->getById($id);

        $img = Html::el('img');
        $img->src = '/images/novinky/' . $news->getMiniature();
        $img->style = 'max-width: 200px;';

        $a = Html::el('a');
        $a->href = '/images/novinky/' . $news->getImage();
        $a->class = 'fancybox';
        $a->title = !empty($news->getTitle()) ? $news->getTitle() . ' - ' : '';

        $a->add($img);

        $this->template->id = $id;
        $this->template->image = $a;
        $data = $news->getAsArray();
        $data['added'] = $news->getAdded()->format('d.m.Y');
        $this['addEditNewsForm']->setDefaults($data);
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditNewsForm()
    {
        $this->addEditNewsFormFactory->setEditing($this->getAction() == 'edit');
        $form = $this->addEditNewsFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Novinka byla úspěšně ' . ($this->getAction() == 'add' ?  'vložena.' : 'editována.'),
                'success'
            );
            $this->redirect('News:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Novinku se nepodařilo ' .
                ($this->getAction() == 'add' ?  'vložit.' : 'editovat.') .
                    '. Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Form
     */
    protected function createComponentEditPageSettingsForm()
    {
        $form = $this->editPageSettingsFormFactory->create();
        $form->setDefaults($this->pageRepository->getOneByParameters(array('url' => PageRepository::NEWS_PAGE_URL))
            ->getAsArray());
        $form->onSuccess[] = function ($form) {
            $this->flashMessage('Nastavení stránky bylo úspěšně změněno.', 'success');
            $this->redirect('this');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage('Nastavení stránky se nepodařilo změnit. Zkuste to prosím znovu.', 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentNewsDataGrid()
    {
        $grid = $this->newsDataGridFactory->create();
        return $grid;
    }
}
