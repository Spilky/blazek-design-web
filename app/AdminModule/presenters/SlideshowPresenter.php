<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\DataGrids\SlideShowPhotosDataGrid;
use App\AdminModule\Forms\AddEditSlideShowPhotoFormFactory;
use App\Model\Repository\SlideShowPhotoRepository;
use Grido\Grid;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Tracy\Debugger;

class SlideshowPresenter extends AdminBasePresenter
{
    /** @var SlideShowPhotoRepository @inject */
    public $slideShowPhotoRepository;
    /** @var AddEditSlideShowPhotoFormFactory @inject */
    public $addEditSlideShowPhotoFormFactory;
    /** @var SlideShowPhotosDataGrid @inject */
    public $slideShowPhotosDataGrid;

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionEdit($id = 0)
    {
        $photo = $this->slideShowPhotoRepository->getById($id);
        if (is_null($photo)) {
            throw new BadRequestException;
        }
    }

    /**
     * @param int $id
     * @throws BadRequestException
     */
    public function actionDelete($id = 0)
    {
        $photo = $this->slideShowPhotoRepository->getById($id);
        if (is_null($photo)) {
            throw new BadRequestException;
        }

        try {
            $this->slideShowPhotoRepository->delete($photo);
            $this->flashMessage('Fotografie ve slideshow úspěšně smazán.', 'success');
        } catch (\Exception $e) {
            $this->flashMessage('Fotografii ve slideshow se nepodařilo smazat. Zkuste to znovu prosím.', 'danger');
            Debugger::log($e);
        }

        $this->redirect('default');
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->flashMessage(
            'Pro správné zobrazení slideshow by měla každá fotografie v ní mít stejný poměr stran.',
            'warning'
        );
    }

    /**
     * @param int $id
     */
    public function renderEdit($id)
    {
        $photo = $this->slideShowPhotoRepository->getById($id);

        $img = Html::el('img');
        $img->src = '/images/slideshow/' . $photo->getImage();
        $img->style = 'max-width: 200px;';

        $a = Html::el('a');
        $a->href = '/images/slideshow/' . $photo->getImage();
        $a->class = 'fancybox';
        $a->title = (!empty($photo->getTitle()) ? $photo->getTitle() . ' - ' : '') . $photo->getDescription();

        $a->add($img);

        $this->template->id = $id;
        $this->template->image = $a;
        $this['addEditSlideShowPhotoForm']->setDefaults($photo->getAsArray());
    }

    /**
     * @return Form
     */
    protected function createComponentAddEditSlideShowPhotoForm()
    {
        $this->addEditSlideShowPhotoFormFactory->setEditing($this->getAction() == 'edit');
        $form = $this->addEditSlideShowPhotoFormFactory->create();
        $form->onSuccess[] = function ($form) {
            $this->flashMessage(
                'Fotografie ve slideshow byla úspěšně ' . ($this->getAction() == 'add' ?  'přidána.' : 'editována.'),
                'success'
            );
            $this->redirect('Slideshow:');
        };
        $form->onError[] = function ($form) {
            $this->flashMessage(
                'Fotografii ve slideshow se nepodařilo ' .
                ($this->getAction() == 'add' ?  'přidat.' : 'editovat.') .
                ' Zkuste to prosím znovu.'
                , 'danger');
        };
        return $form;
    }

    /**
     * @return Grid
     */
    protected function createComponentSlideShowPhotosDataGrid()
    {
        $grid = $this->slideShowPhotosDataGrid->create();
        return $grid;
    }
}
