<?php

namespace App\FrontModule\Forms;

use App\Model\Repository\ContactRepository;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class ContactForm
{
    /** @var ContactRepository */
    private $contactRepository;
    /** @var LinkGenerator */
    private $linkGenerator;
    /** @var SmtpMailer */
    private $mailer;

    public function __construct(
        ContactRepository $contactRepository,
        LinkGenerator $linkGenerator,
        SmtpMailer $mailer
    ) {
        $this->contactRepository = $contactRepository;
        $this->linkGenerator = $linkGenerator;
        $this->mailer = $mailer;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = new Form;

        $form->addText('name')
            ->setAttribute('placeholder', 'Jméno')
            ->setRequired('Vložte prosím jméno.');

        $form->addText('email')
            ->setAttribute('placeholder', 'E-mail')
            ->setRequired('Vložte prosím e-mail.')
            ->addRule(Form::EMAIL, 'Vložte prosím validní e-mail.');

        $form->addText('telephone')
            ->setAttribute('placeholder', 'Telefon');

        $form->addText('subject')
            ->setAttribute('placeholder', 'Předmět');

        $form->addTextArea('message')
            ->setAttribute('placeholder', 'Zpráva')
            ->setRequired('Vložte prosím zprávu.');

        $form->addSubmit('submit', 'Odeslat');

        $form->onSuccess[] = array($this, 'formSucceeded');
        return $form;
    }

    /**
     * @param Form $form
     * @param ArrayHash $values
     */
    public function formSucceeded(Form $form, ArrayHash $values)
    {
        $mail = new Message();
        foreach ($this->contactRepository->getBrandedAll() as $contact) {
            if ($contact->getContactForm()) {
                $mail->addTo($contact->getEmail());
            }
        }

        $mail->addReplyTo($values['email']);
        $mail->setFrom('Blažek design <admin@truhlarstvijps.cz>')
            ->setSubject('Zpravá z kontaktního formuláře - ' . $values['subject'])
            ->setBody("Dobrý den,\nNěkdo odeslal zpráv přes kontaktní formulář na stránce " .
                $this->linkGenerator->link('Front:Homepage:') . ":\n" .
                "Jméno: " . $values['name'] . "\n" .
                "E-mail: " . $values['email'] . "\n" .
                "Telefon: " . $values['telephone'] . "\n" .
                "Předmět: " . $values['subject'] . "\n" .
                "Zpráva: " . $values['message']);

        try {
            $this->mailer->send($mail);
        } catch (\Exception $e) {
            $form->addError($e->getMessage());
            Debugger::log($e);
        }
    }
}