<?php

namespace App\FrontModule\Presenters;

use App\Model\Repository\PageRepository;
use App\Model\Repository\ServiceRepository;

class ServicesPresenter extends FrontBasePresenter
{
    /** @var ServiceRepository @inject */
    public $serviceRepository;
    /** @var PageRepository @inject */
    public $pageRepository;

    public function renderDefault()
    {
        $serviceEntities = $this->serviceRepository->getBrandedAll();
        $services = array();
        foreach ($serviceEntities as $entity) {
            $services[] = $entity->getAsArray();
        }
        $this->template->services = $services;

        $this->template->servicesSettings = $this->pageRepository->getOneByParameters(array('url' => 'sluzby'))->getAsArray();
    }
}
