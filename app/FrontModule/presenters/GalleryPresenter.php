<?php

namespace App\FrontModule\Presenters;

use App\Model\Repository\PageRepository;
use App\Model\Repository\PhotoCategoryRepository;
use App\Model\Repository\PhotoRepository;
use App\Model\Service\ImageManager;
use Nette;
use App\Model;

class GalleryPresenter extends FrontBasePresenter
{
    /** @var PhotoRepository @inject */
    public $photoRepository;
    /** @var PhotoCategoryRepository @inject */
    public $photoCategoryRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var ImageManager @inject */
    public $imageManager;

    public function renderDefault()
    {
        $photoEntities = $this->photoRepository->getQB()->orderBy('table.title')->getQuery()->getResult();
        $photos = array();
        foreach ($photoEntities as $entity) {
            $photos[] = $entity->getAsArray();
        }
        $this->template->photos = $photos;

        $categoryEntities = $this->photoCategoryRepository->getBrandedAll();
        $categories = array();
        foreach ($categoryEntities as $entity) {
             $categories[] = $entity->getAsArray();
        }
        $this->template->categories = $categories;

        $page = $this->pageRepository->getOneByParameters(array('url' => 'galerie'));
        $this->template->gallerySettings = $page->getAsArray();
        $this->template->defaultCategory = 0;

        $customInfo = $page->getCustomInfo();
        if (isset($customInfo['category']) && $customInfo['category'] != 0) {
            foreach ($categoryEntities as $entity) {
                if ($entity->getId() == $customInfo['category']) {
                    $this->template->defaultCategory = $customInfo['category'];
                    break;
                }
            }
        }
    }
}
