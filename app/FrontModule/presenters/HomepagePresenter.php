<?php

namespace App\FrontModule\Presenters;

use App\Model\Entity\News;
use App\Model\Repository\NewsRepository;
use App\Model\Repository\PageRepository;
use App\Model\Repository\PhotoRepository;
use App\Model\Repository\SlideShowPhotoRepository;

class HomepagePresenter extends FrontBasePresenter
{
    /** @var NewsRepository @inject */
    public $newsRepository;
    /** @var PhotoRepository @inject */
    public $photoRepository;
    /** @var SlideShowPhotoRepository @inject */
    public $slideShowPhotoRepository;
    /** @var PageRepository @inject */
    public $pageRepository;

    public function actionDefault()
    {
        if (!is_null($this->subUrl)) {
            $this->setView('subUrlDefault');
        }
    }

    public function renderDefault()
    {
        $brands = $this->brandRepository->getAll();
        $slideshowPhotoEntities = $this->slideShowPhotoRepository->getAll();
        $slideshowPhotos = [];
        $texts = [];
        foreach ($brands as $brand) {
            $slideshowPhotos[$brand->getSubUrl()] = [];
            $texts[$brand->getSubUrl()] = $brand->getText();
        }
        foreach ($slideshowPhotoEntities as $entity) {
            $slideshowPhotos[$entity->getBrand()->getSubUrl()][] = $entity->getAsArray();
        }
        $this->template->texts = $texts;
        $this->template->slideshowPhotos = $slideshowPhotos;
    }

    public function renderSubUrlDefault()
    {
        $slideshowPhotoEntities = $this->slideShowPhotoRepository->getBrandedAll();
        $slideshowPhotos = array();
        foreach ($slideshowPhotoEntities as $entity) {
            $slideshowPhotos[] = $entity->getAsArray();
        }
        $this->template->slideshowPhotos = $slideshowPhotos;

        $newsEntities = $this->newsRepository->getBrandedAll();
        $news = array();
        foreach ($newsEntities as $entity) {
            /** @var News $entity */
            $news[$entity->getAdded()->getTimestamp() . $entity->getId()] = $entity->getAsArray();
        }
        krsort($news);
        $this->template->news = $news;
        $this->template->newsSettings = $this->pageRepository->getOneByParameters(array('url' => 'novinky'))->getAsArray();

        $photoEntities = $this->photoRepository->getByParameters(array('homePage' => true));
        $photos = array();
        foreach ($photoEntities as $entity) {
            $photos[] = $entity->getAsArray();
        }
        $this->template->photos = $photos;
        $this->template->gallerySettings = $this->pageRepository->getOneByParameters(array('url' => 'galerie'))->getAsArray();
    }
}
