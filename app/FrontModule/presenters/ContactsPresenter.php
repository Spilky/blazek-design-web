<?php

namespace App\FrontModule\Presenters;

use App\FrontModule\Forms\ContactForm;
use App\Model\Repository\ContactRepository;
use App\Model\Repository\PageRepository;
use Nette\Application\UI\Form;

class ContactsPresenter extends FrontBasePresenter
{
    /** @var ContactRepository @inject */
    public $contactRepository;
    /** @var PageRepository @inject */
    public $pageRepository;
    /** @var ContactForm @inject */
    public $contactForm;

    public function renderDefault()
    {
        $contactEntities = $this->contactRepository->getBrandedAll();
        $contacts = array();
        foreach ($contactEntities as $entity) {
            $contacts[] = $entity->getAsArray();
        }
        $this->template->contacts = $contacts;

        $page = $this->pageRepository->getOneByParameters(array('url' => 'kontakty'));
        $this->template->contactsSettings = $page->getAsArray();
        $customInfo = $page->getCustomInfo();
        $this->template->contactAddress = isset($customInfo['contactAddress']) ? $customInfo['contactAddress'] : [];
        $this->template->billingAddress = isset($customInfo['billingAddress']) ? $customInfo['billingAddress'] : [];;
    }

    /**
     * @return Form
     */
    protected function createComponentContactForm()
    {
        $form = $this->contactForm->create();
        $form->onSuccess[] = function (Form $form) {
            $form->setValues([], true);
            $this->template->formSended = true;
            $this->template->successMessages = [];
            $this->template->successMessages[] = 'Kontaktní formulář byl úspěšně odeslán. Brzy se Vám ozveme.';
            $this->redrawControl('contact-form');
        };
        $form->onError[] = function (Form $form) {
            $this->template->formSended = true;
            $this->template->dangerMessages = [];
            $this->template->dangerMessages[] = 'Kontaktní formulář se nepodařilo odeslat. ' .
                ($form->getOwnErrors() ?
                    'Zkuste to prosím znovu.' : 'Zkuste prosím opravit vyplněné údaje a znovu ho odeslat.');
            foreach (array_diff($form->getErrors(), $form->getOwnErrors()) as $error) {
                $this->template->dangerMessages[] = $error;
            }
            $this->redrawControl('contact-form');
        };
        return $form;
    }
}
