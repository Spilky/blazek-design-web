<?php

namespace App\FrontModule\Presenters;

use App\Model\Repository\BrandRepository;
use App\Model\Service\BrandedTablesManager;
use App\Presenters\BasePresenter;
use Nette\Application\BadRequestException;

/**
 * Base presenter for all FrontModule presenters.
 */
abstract class FrontBasePresenter extends BasePresenter
{
    /** @var number @persistent */
    public $subUrl = null;
    /** @var BrandRepository @inject */
    public $brandRepository;

    public function startup()
    {
        parent::startup();
        if (!is_null($this->subUrl)) {
            $brand = $this->brandRepository->getOneByParameters(array('subUrl' => $this->subUrl));
            BrandedTablesManager::setBrand($brand);
            if (!is_null($brand)) {
                return;
            }
        } elseif ($this->presenter->getName() == 'Front:Homepage' && $this->presenter->getAction() == 'default') {
            return;
        }

        throw new BadRequestException;
    }

    public function beforeRender()
    {
        if (!is_null($this->subUrl)) {
            $this->template->brandName = BrandedTablesManager::getBrand()->getName();
        }
    }
}
